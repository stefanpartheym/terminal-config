################################################################################
# terminal environment configuration
################################################################################

USER_HOME          := ~

SOURCE_DIR         := ./configuration
DESTINATION_DIR    := $(USER_HOME)
CONFIG_DIR_ZSH     := zsh
CONFIG_PATH_ZSH    := $(DESTINATION_DIR)/.$(CONFIG_DIR_ZSH)
DOT_FILE_ZSHRC     := zshrc
DOT_FILE_GITCONFIG := gitconfig
DOT_FILE_VIMRC     := vimrc
DOT_FILE_TMUX      := tmux.conf

CP                 := cp -rf

default:
	@echo "NOTE: Type 'make update' to update configuration files from local "\
          "installation into this repository"

update:
	$(CP) $(DESTINATION_DIR)/.$(DOT_FILE_ZSHRC)     $(SOURCE_DIR)/$(DOT_FILE_ZSHRC)
	$(CP) $(DESTINATION_DIR)/.$(DOT_FILE_GITCONFIG) $(SOURCE_DIR)/$(DOT_FILE_GITCONFIG)
	$(CP) $(DESTINATION_DIR)/.$(DOT_FILE_VIMRC)     $(SOURCE_DIR)/$(DOT_FILE_VIMRC)
	$(CP) $(DESTINATION_DIR)/.$(DOT_FILE_TMUX)      $(SOURCE_DIR)/$(DOT_FILE_TMUX)
	$(CP) $(CONFIG_PATH_ZSH)/custom $(SOURCE_DIR)/$(CONFIG_DIR_ZSH)/

.PHONY: default update
