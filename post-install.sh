#!/bin/bash

################################################################################
# Installation of required homebrew packages.
################################################################################

function step {
    echo "$1:"
}

function result {
    if [ $? -eq 0 ]; then
        echo "  => SUCCESS"
    else
        echo "  => FAILED"
        exit 1
    fi
}

# Install tmux
brew install tmux
# Install iTerm2 terminal emulator
brew cask install iterm2

# Change default login shell to zsh installed by homebrew
chsh -s /bin/zsh

# Install custom terminal configuration template
step "Installing custom terminal configuration template"
cp "./templates/zsh/custom/customrc" "${HOME}/.zsh/custom/"
result
