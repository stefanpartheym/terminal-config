#!/bin/bash

################################################################################
# Installation script for basic terminal environment configuration.
################################################################################

function step {
    echo "$1:"
}

function result {
    if [ $? -eq 0 ]; then
        echo "  => SUCCESS"
    else
        echo "  => FAILED"
        exit 1
    fi
}

source_dir=./configuration
destination_dir=~

config_dir_zsh="zsh"
config_path_zsh="${destination_dir}/.${config_dir_zsh}"

antigen_clone_url="https://github.com/zsh-users/antigen.git"
antigen_dest_path="${destination_dir}/.${config_dir_zsh}/antigen"

dot_file_zshrc="zshrc"
dot_file_vimrc="vimrc"
dot_file_tmux="tmux.conf"

dot_file_gitconfig="gitconfig"

declare -a dot_files=(
    $dot_file_zshrc
    $dot_file_vimrc
    $dot_file_tmux
)

# Setup git configuration
step "Configure git"
read -p "Git user email: " git_user_email && \
read -p "Git user name: " git_user_name
result

dot_file_gitconfig_content=`cat "${source_dir}/${dot_file_gitconfig}"`
dot_file_gitconfig_content="${dot_file_gitconfig_content/@git.email@/$git_user_email}"
dot_file_gitconfig_content="${dot_file_gitconfig_content/@git.name@/$git_user_name}"

# Create zsh config directory
step "Creating directory \"$config_path_zsh\""
mkdir -p "$config_path_zsh"
result

# Install dot-files
for file in "${dot_files[@]}"; do
    step "Installing \"${file}\""
    install -m 644 "${source_dir}/${file}" "${destination_dir}/.${file}"
    result
done

# Install gitconfig
step "Installing \"gitconfig\""
echo -n "$dot_file_gitconfig_content" > "${destination_dir}/.${dot_file_gitconfig}"
result

# Install zsh config files
step "Installing zsh config files to \"$config_path_zsh\""
cp -rf "${source_dir}/${config_dir_zsh}"/** "$config_path_zsh"
result

# Install Antigen
step "Installing Antigen plugin manager to \"$antigen_dest_path\""
git clone "$antigen_clone_url" "$antigen_dest_path"
result
